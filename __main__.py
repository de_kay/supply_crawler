import sys

from vendor.onch.OnchWholeDataParser import whole_main
from vendor.onch.OnchDailyDataParser import daily_main
from vendor.onch.OnchSoldoutDataParser import set_sold_out


if __name__ == '__main__':
    if sys.argv[1] == 'onch':
        if sys.argv[2] == 'daily_main':
            daily_main()
        elif sys.argv[2] == 'sold_out':
            set_sold_out()
        elif sys.argv[2] == 'whole_main':
            whole_main(sys.argv[3])
        else:
            print('PARAMETER UNKNOWN')
    else:
        print('VENDOR NAME UNKNOWN')

from vendor.common.Logger import Logger

logger = Logger().logger()


class FileDownload:

    def __init__(self, session, url, r_url, headers, params, path, file_name, redirect):
        self.session = session
        self.url = url
        self.r_url = r_url
        self.headers = headers
        self.params = params
        self.path = path
        self.file_name = file_name
        self.redirect = redirect

    def download_post(self):
        with self.session as s:
            if self.redirect:
                s.post(self.url, headers=self.headers, data=self.params, allow_redirects=False)
                with open(self.path, "wb") as file:
                    response = s.get(self.r_url)
                    file.write(response.content)
            else:
                with open(self.path, "wb") as file:
                    response = s.post(self.url, headers=self.headers, data=self.params, allow_redirects=False)
                    file.write(response.content)
        logger.info('다운로드 완료')

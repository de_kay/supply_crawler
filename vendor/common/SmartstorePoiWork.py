from bs4 import BeautifulSoup
from openpyxl import load_workbook

from vendor.common.ConvertToXlsx import ConvertToXlsx
from vendor.common.Logger import Logger
from vendor.onch.mapper.OnchMapper import insert_option, duplication_check
from vendor.onch.mapper.OnchMapper import get_catecd

logger = Logger().logger()


class SmartstorePoiWork:

    def __init__(self, path, sheet_name, scm_no, row_cnt, goods_dic):
        self.path = path
        self.sheet_name = sheet_name
        self.scm_no = scm_no
        self.row_cnt = row_cnt
        self.goods_dic = goods_dic

    def parser(self):
        new_path = ConvertToXlsx(self.path).doConvert()
        load_wb = load_workbook(new_path, data_only=True)
        load_ws = load_wb[self.sheet_name]
        goods_list = []

        for i in range(2, self.row_cnt):
            goods_model = []

            if load_ws.cell(i, 1).value is None or load_ws.cell(i, 1).value == '':
                break;

            if load_ws.cell(i, 50).value != '조합형':  # 조합형 옵션이 아닌 상품은 믿거다.
                continue

            goods_cd = load_ws.cell(i, 11).value
            duple_cnt = duplication_check(goods_cd)
            if int(duple_cnt[0]) > 0:
                continue

            goods_model.append(load_ws.cell(i, 3).value)  # goodsNm
            goods_model.append(self.scm_no)  # scmNo

            goods_model.append(goods_cd)  # goodsCd

            onch_cate_cd = load_ws.cell(i, 2).value
            cate_cd = get_catecd(onch_cate_cd)

            # 맵핑할 카테고리 코드가 없을경우 넘어감.
            if cate_cd is None or cate_cd == '':
                continue

            goods_model.append(cate_cd)  # cateCd
            goods_model.append('')  # goodsSearchWord 엑셀로는 제공하지 않음.
            goods_model.append(load_ws.cell(i, 13).value)  # makerNm
            goods_model.append(load_ws.cell(i, 23).value)  # originNm
            goods_model.append('0')  # stockMod 0: 재고있음, 1: 품절
            goods_model.append('0')  # minOrderCnt 최소 구매수량
            goods_model.append('0')  # maxOrderCnt 최대 구매수량

            goods_price = load_ws.cell(i, 4).value
            if goods_price is None or goods_price == '':
                continue

            goods_model.append(goods_price)  # goodsPrice
            goods_model.append('0')  # fixedPrice 가격 자율이므로 0
            goods_model.append(goods_price)  # costPrice

            option_name = load_ws.cell(i, 51).value
            goods_model.append(option_name)  # optionName

            goods_description = load_ws.cell(i, 10).value
            goods_model.append(goods_description)  # goodsDescription

            deli_type = '1' if load_ws.cell(i, 25).value == '무료' else '3'
            goods_model.append(deli_type)  # deliType

            deli_method = '0' if load_ws.cell(i, 27).value == '선결제' else '1'
            goods_model.append(deli_method)  # deliMethod

            goods_model.append(load_ws.cell(i, 26).value)  # deliPrice

            try:
                image_name = self.goods_dic[goods_cd]
            except KeyError as e:
                logger.error(e)
                continue

            goods_model.append(image_name)  # imageName url이 아니라 파일을 제공하므로 dic에서 검색
            goods_model.append('0')  # overseasFl

            refund_msg = refund_check(goods_description)
            refund_fl = '0' if refund_msg.find('절대불가') > 0 else '1'
            goods_model.append(refund_fl)  # refundFl
            goods_model.append(refund_msg)  # refundMsg

            adult_fl = '0' if load_ws.cell(i, 18).value == 'Y' else '1'
            goods_model.append(adult_fl)  # adultFl

            if option_name is not None and option_name != '':

                option_value = load_ws.cell(i, 52).value
                if option_value is None or option_value == '':
                    continue

                option_price = load_ws.cell(i, 53).value
                if option_price is None or option_price == '':
                    continue

                option_stock = load_ws.cell(i, 54).value
                if option_stock is None or option_stock == '':
                    continue

                option_parser(goods_cd, str(option_value), str(option_price), str(option_stock))

            goods_list.append(goods_model)

        return goods_list


def refund_check(goods_description):
    doc = BeautifulSoup(goods_description, 'html.parser')
    return doc.find_all('p')[0].text


def option_parser(goods_cd, option_value, option_price, option_stock):
    option_list = []
    for i in range(0, len(option_value.split(','))):
        option_id = option_value.split(',')
        option_prices = option_price.split(',')
        option_stocks = option_stock.split(',')

        option_model = [option_id[i], goods_cd, option_prices[i], option_stocks[i], option_stocks[i], '1']

        option_list.append(option_model)

    insert_option(option_list)

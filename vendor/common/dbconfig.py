import pymysql


class DBController:
    HOST = '211.223.105.54'
    ID = 'test'
    PW = 'test12!'
    DB_NAME = 'SELLDOUM_VENDOR'

    def __init__(self):
        self.conn = pymysql.connect(
            host=DBController.HOST,
            user=DBController.ID,
            password=DBController.PW,
            db=DBController.DB_NAME,
            charset='utf8')
        self.curs = self.conn.cursor()

    def queringOne(self, sql):
        self.curs.execute(sql)
        self.conn.commit()

    def quering(self, sql, model):
        self.curs.execute(sql, model)
        self.conn.commit()

    def queringmany(self, sql, model):
        self.curs.executemany(sql, model)
        self.conn.commit()

    def selectOne(self, sql, model):
        self.curs.execute(sql, model)
        return self.curs.fetchone()
import logging
from logging.handlers import RotatingFileHandler

logger = logging.getLogger(__name__)


class Logger:

    @classmethod
    def logger(cls):
        if logger.hasHandlers():
            logger.handlers.clear()

        fileHandler = RotatingFileHandler(
            '/home/bridgezone/SELLDOUM_CRAWLER/logging.out',
            maxBytes=1024 * 1024 * 1024,
            backupCount=100
        )
        fileHandler.setFormatter(
            logging.Formatter('%(asctime)s [%(levelname)s] [%(filename)s:%(lineno)s] >> %(message)s'))
        logger.addHandler(fileHandler)
        logger.setLevel(logging.DEBUG)
        return logger

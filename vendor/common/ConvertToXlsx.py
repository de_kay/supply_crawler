import xlrd
from openpyxl.workbook import Workbook


class ConvertToXlsx:

    def __init__(self, path):
        self.path = path

    def doConvert(self):
        book_xls = xlrd.open_workbook(self.path + '.xls')
        book_xlsx = Workbook()

        sheet_names = book_xls.sheet_names()
        for sheet_index, sheet_name in enumerate(sheet_names):
            sheet_xls = book_xls.sheet_by_name(sheet_name)
            if sheet_index == 0:
                sheet_xlsx = book_xlsx.active
                sheet_xlsx.title = sheet_name
            else:
                sheet_xlsx = book_xlsx.create_sheet(title=sheet_name)

            for row in range(0, sheet_xls.nrows):
                for col in range(0, sheet_xls.ncols):
                    sheet_xlsx.cell(row=row + 1, column=col + 1).value = sheet_xls.cell_value(row, col)
        new_path = self.path + '.xlsx'
        book_xlsx.save(new_path)
        return new_path
from vendor.common import dbconfig


def insert_goods(model):
    sql = 'INSERT INTO shop_goods_crawling (' \
          'goodsNm' \
          ', scmNo' \
          ', goodsCd' \
          ', cateCd' \
          ', goodsSearchWord' \
          ', makerNm' \
          ', originNm' \
          ', stockMod' \
          ', minOrderCnt' \
          ', maxOrderCnt' \
          ', goodsPrice' \
          ', fixedPrice' \
          ', costPrice' \
          ', optionName' \
          ', goodsDescription' \
          ', deliType' \
          ', deliMethod' \
          ', deliPrice' \
          ', imageName' \
          ', overseasFl' \
          ', refundFl' \
          ', refundMsg' \
          ', adultFl' \
          ', regDt' \
          ', modDt' \
          ') VALUES (' \
          '%s' \
          ',%s' \
          ',%s' \
          ',%s' \
          ',%s' \
          ',%s' \
          ',%s' \
          ',%s' \
          ',%s' \
          ',%s' \
          ',%s' \
          ',%s' \
          ',%s' \
          ',%s' \
          ',%s' \
          ',%s' \
          ',%s' \
          ',%s' \
          ',%s' \
          ',%s' \
          ',%s' \
          ',%s' \
          ',%s' \
          ',NOW()' \
          ',NOW()' \
          ')'

    inst = dbconfig.DBController()
    inst.queringmany(sql, model)


def insert_option(model):
    sql = 'INSERT INTO shop_goods_option_crawling (' \
          'io_id' \
          ', gs_id' \
          ', io_price' \
          ', io_stock_qty' \
          ', io_noti_qty' \
          ', io_use' \
          ') VALUES (' \
          '%s' \
          ',%s' \
          ',%s' \
          ',%s' \
          ',%s' \
          ',%s' \
          ')'
    inst = dbconfig.DBController()
    inst.queringmany(sql, model)


def get_catecd(cate_cd):
    sql = 'SELECT cateCd FROM CATEGORY_CODE_MAP WHERE smartstore = %s LIMIT 1'
    inst = dbconfig.DBController()
    result = inst.selectOne(sql, cate_cd)
    return result


def soldout(model):
    sql = 'UPDATE shop_goods_crawling SET stockMod = "1" WHERE goodsCd = %s'
    inst = dbconfig.DBController()
    inst.queringmany(sql, model)


def update_option_goodsno():
    sql = 'UPDATE ' \
          'shop_goods_option_crawling AS a ' \
          'INNER JOIN shop_goods_crawling AS b ' \
          'ON a.gs_id = b.goodsCd ' \
          'SET ' \
          'a.gs_id = b.goodsNo'
    inst = dbconfig.DBController()
    inst.queringOne(sql)


def duplication_check(goods_cd):
    sql = 'SELECT COUNT(*) AS cnt FROM shop_goods_crawling WHERE goodsCd = %s'
    inst = dbconfig.DBController()
    result = inst.selectOne(sql, goods_cd)
    return result

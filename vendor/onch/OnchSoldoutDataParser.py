from bs4 import BeautifulSoup

from vendor.common.Logger import Logger
from vendor.onch.OnchLoginService import Onchloginservice
from vendor.onch.mapper.OnchMapper import soldout

SOLDOUT_URL = 'http://www.onch3.co.kr/admin_mem_clo_list.html'
logger = Logger().logger()


def set_sold_out():
    session = Onchloginservice().get_session()

    q = 0
    for i in range(1, 1000):
        params = {
            'page': i
        }
        response = session.get(SOLDOUT_URL, params=params)
        response.encoding = 'UTF-8'
        responseBody = response.text
        doc = BeautifulSoup(responseBody, 'html.parser')
        void_tbody = doc.find_all(string="등록된 상품이 없습니다.")

        if len(void_tbody) == 1:
            logger.info('마지막 페이지에 도달했습니다.')
            break

        tds = doc.find_all('td', class_='title_2 sub_title')
        model = []
        for td in tds:
            btags = td.find_all('b')
            for b in btags:
                if b.text[0:2] != 'CH':
                    continue
                model.append(b.text)
                q += 1

        soldout(model)

    logger.info('품절 검색 완료 :: COUNT => ' + str(q))

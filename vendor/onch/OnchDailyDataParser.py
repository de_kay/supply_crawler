import requests
from bs4 import BeautifulSoup
from datetime import date, timedelta

from vendor.common.FileDownload import FileDownload
from vendor.common.Logger import Logger
from vendor.common.SmartstorePoiWork import SmartstorePoiWork
from vendor.onch.OnchLoginService import Onchloginservice
from vendor.onch.OnchSoldoutDataParser import set_sold_out
from vendor.onch.OnchZipIOModule import OnchZipIOModule
from vendor.onch.mapper.OnchMapper import insert_goods, update_option_goodsno

logger = Logger().logger()


def daily_main():
    logger.info('ONCH DAILY DATA PARSING LET US KILLING BEGIN!')
    ydate = date.today() - timedelta(1)
    yday = ydate.strftime('%Y-%m-%d')
    # yday = '2020-11-29'

    URL = 'http://www.onch3.co.kr/dbcenter_renewal/excel_download_center.html'
    FILE_NAME = 'happy8778'
    session = Onchloginservice().get_session()
    response = session.get(URL + '?cate=rule1')
    response.encoding = 'UTF-8'
    responseBody = response.text
    doc = BeautifulSoup(responseBody, 'html.parser')
    # page_info = doc.find('p', class_='page_info').text
    # last_page = page_info.split('/')[1]
    last_page = '1000'

    conti_process = True

    for i in range(1, int(last_page)):
        if not conti_process:
            break

        logger.info(str(i) + " page searching...")
        params = {
            "cate": "rule1",
            "page": i
        }
        response = session.get(URL, params=params)
        response.encoding = 'UTF-8'
        responseBody = response.text
        doc = BeautifulSoup(responseBody, 'html.parser')
        product_sets = doc.select('.product_set')

        goods_dic = {}
        goods_cds = []

        for product_set in product_sets:
            date_content = product_set.find('dd', class_='product_date')
            goods_date = date_content.find('span', class_='ps_content').text
            if goods_date != yday:
                logger.info('입점일자 바뀜. 검색을 종료합니다.')
                conti_process = False
                break

            state_content = product_set.find('dd', class_='product_state')
            goods_state = state_content.find('span', class_='ps_content').text
            if goods_state != '정상판매':
                logger.warn('정상판매 상태가 아닙니다.')
                continue

            price_content = product_set.find('dd', class_='product_price')
            goods_price = price_content.find('span', class_='ps_content').text
            if goods_price == '0원':
                logger.info('품절입니다.')
                continue

            goods_cd = product_set.find('span', class_='code_num').text
            goods_cds.append('list_countArr=' + goods_cd)
            goods_img = product_set.find('img', class_='product_img_thumbnail').get('src').replace("/./", "/", 1)
            goods_dic[goods_cd] = goods_img

        if len(goods_cds) == 0:
            continue

        url = 'http://www.onch3.co.kr/dbcenter_renewal/process/excel_download_access_storefarm.php?ubr=excel_download'
        goods_cds_string = '&'.join(goods_cds)
        headers = {
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
            'Accept': 'text/plain, */*; q=0.01',
            'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) ' \
                          'Chrome/83.0.4103.116 Safari/537.36'
        }

        params = {
            'sel_op': '1',
            'sear_choice': '',
            'cate_sec': 'storefarm',
            'my_frm_download': '1006',
            'list_count': goods_cds_string,
            'filename': FILE_NAME
        }
        file_name = FILE_NAME + '.zip'
        r_url = 'http://www.onch3.co.kr/dbcenter_renewal/ExcelData/zip_files/' + file_name
        parent_path = '/home/bridgezone/SELLDOUM_CRAWLER/files/onch/'
        path = parent_path + file_name

        try:
            download = FileDownload(session, url, r_url, headers, params, path, file_name, True)
            download.download_post()
        except requests.exceptions.ConnectionError as e:
            logger.error(e)
            logger.error('세션이 종료되었습니다. 다시 연결합니다...')
            session = Onchloginservice().get_session()
            download = FileDownload(session, url, r_url, headers, params, path, file_name, True)
            download.download_post()

        poi_path = OnchZipIOModule(parent_path, FILE_NAME).unzip()
        model = SmartstorePoiWork(poi_path, 'onchExcel', 'AP-100018', 202, goods_dic).parser()
        insert_goods(tuple(model))
        update_option_goodsno()

    logger.info(yday + 'COMPLETE DAILY GOODS UPDATE')

    set_sold_out()
    logger.info(yday + 'COMPLETE SOLD OUT LIST UPDATE')
    logger.info(yday + ' : ONCH BYE')

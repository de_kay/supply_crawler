import requests
from bs4 import BeautifulSoup


class Onchloginservice:
    LOGIN_URL = 'https://login.onch3.co.kr/on_auth/login_onch/KOR'
    AUTH_URL = 'http://www.onch3.co.kr/__main/across_auth.php'
    ENCODING_TYPE = 'UTF-8'
    HEADERS = {
        'Content-Type': 'application/x-www-form-urlencoded',
        'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) ' \
                      'Chrome/83.0.4103.116 Safari/537.36'
    }
    ID = 'happy8778'
    PW = 'jhy1998'

    def __init__(self):
        self.LOGIN_URL = Onchloginservice.LOGIN_URL
        self.AUTH_URL = Onchloginservice.AUTH_URL
        self.ENCODING_TYPE = Onchloginservice.ENCODING_TYPE
        self.HEADERS = Onchloginservice.HEADERS
        self.ID = Onchloginservice.ID
        self.PW = Onchloginservice.PW

    def get_session(self):
        reqBody = {
            'login_url': '',
            'login': self.ID,
            'password': self.PW
        }
        session = requests.session()
        response = session.post(self.LOGIN_URL, data=reqBody, headers=self.HEADERS)
        responseBody = response.text
        doc = BeautifulSoup(responseBody, 'html.parser')
        ssid = doc.find('input', {'name': 'ssid'}).get('value')
        goto = doc.find('input', {'name': 'goto'}).get('value')

        reqBody = {
            'ssid': ssid,
            'goto': goto
        }
        response = session.post(self.AUTH_URL, data=reqBody, headers=self.HEADERS)
        return session

import zipfile
import shutil
import os


class OnchZipIOModule:

    def __init__(self, path, file_name):
        self.path = path
        self.file_name = file_name
        self.full_path = path + file_name

    def unzip(self):
        with zipfile.ZipFile(self.full_path + '.zip', 'r') as zf:

            for member in zf.namelist():
                in_file_name = os.path.basename(member)

                if not in_file_name:
                    continue

                if member.find(self.file_name + '.xls') > 0:
                    source = zf.open(member)
                    target = open(os.path.join(self.path, in_file_name), "wb")
                    with source, target:
                        shutil.copyfileobj(source, target)
        os.remove(self.full_path + '.zip')
        return self.full_path
